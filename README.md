## find-mi-bot

### before:

 - install java 17, f.e. from [here](https://docs.aws.amazon.com/corretto/latest/corretto-17-ug/downloads-list.html)
 - install docker engine, f.e. from [here](https://docs.docker.com/engine/install/)
 - register your telegram bot as described [here](https://core.telegram.org/bots#how-do-i-create-a-bot)
 - create .env file in a root folder with two variables: 
 
 > BOT_NAME={your_bot_name}  
 > BOT_API_KEY={your_token}

---

### after:

 - to run locally:
   - run `sudo docker compose up -d`
   - run `MICRONAUT_ENVIRONMENTS=local ./gradlew clean run`
 - to test:
   - run `MICRONAUT_ENVIRONMENTS=test ./gradlew clean test`
 - to create a Docker image:
   - run `sudo docker image build -t {image_name}:{tag_name} .`
 - to run app in a Docker container after image has been built:
   - run `sudo docker container run --name {container_name} --env-file=.env {image_name}:{tag_name}`