package com.thoughtsporadic.findmibot.bot.util;

import com.thoughtsporadic.findmibot.bot.AbstractUnitTest;
import com.thoughtsporadic.findmibot.bot.TestFileUtils;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendVoice;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

class BotMessageHelperUnitTest extends AbstractUnitTest {

    @Inject
    BotMessageHelper sut;

    @Test
    void givenProperMessageAndText_whenCreateMessage_thenSuccess() {
        //given
        String text = "test";
        Message testMessage = new Message();
        testMessage.setChat(new Chat(13L, "testChatName"));
        testMessage.setMessageId(42);

        //when
        SendMessage actual = sut.createMessage(testMessage, text);

        //then
        assertThat(actual)
                .isNotNull()
                .hasFieldOrPropertyWithValue("chatId", "13")
                .hasFieldOrPropertyWithValue("replyToMessageId", 42)
                .hasFieldOrPropertyWithValue("text", "test");
    }

    @Test
    void givenProperVoiceFile_whenCreateVoiceMessage_thenSuccess() throws IOException {
        //given
        String filepath = "test_voice.mpga";
        byte[] expectedVoice = TestFileUtils.getFileAsBytes(filepath);
        Message testMessage = new Message();
        testMessage.setChat(new Chat(13L, "testChatName"));
        testMessage.setMessageId(42);

        //when
        SendVoice actual = sut.createVoice(testMessage, filepath);
        byte[] actualVoice = actual.getVoice()
                .getNewMediaStream()
                .readAllBytes();

        //then
        assertThat(actual)
                .isNotNull()
                .hasFieldOrPropertyWithValue("chatId", "13")
                .hasFieldOrPropertyWithValue("replyToMessageId", 42);
        assertThat(actualVoice)
                .isEqualTo(expectedVoice);
    }
}