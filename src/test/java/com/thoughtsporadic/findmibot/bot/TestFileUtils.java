package com.thoughtsporadic.findmibot.bot;

import com.thoughtsporadic.findmibot.bot.exception.TestFileUtilsException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

public class TestFileUtils {

    public static byte[] getFileAsBytes(String filename) {
        try (final InputStream is = TestFileUtils.class.getClassLoader().getResourceAsStream(filename)) {
            return Objects.requireNonNull(is).readAllBytes();
        } catch (IOException e) {
            e.printStackTrace();
            throw new TestFileUtilsException("Cannot read a file", e);
        }
    }
}
