package com.thoughtsporadic.findmibot;

import com.thoughtsporadic.findmibot.bot.TelegramBot;
import com.thoughtsporadic.findmibot.bot.constants.Environments;
import io.micronaut.context.ApplicationContext;
import io.micronaut.runtime.Micronaut;

public class Application {

    public static void main(String[] args) {
        try {
            final ApplicationContext appContext = Micronaut.build(args)
                    .banner(false)
                    .eagerInitSingletons(true)
                    .build().start();
            final boolean isTest = appContext.getEnvironment()
                    .getActiveNames()
                    .contains(Environments.TEST);
            if (!isTest) {
                appContext.getBean(TelegramBot.class).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}