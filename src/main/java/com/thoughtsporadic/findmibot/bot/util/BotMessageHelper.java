package com.thoughtsporadic.findmibot.bot.util;

import jakarta.inject.Singleton;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendVoice;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.io.InputStream;

@Singleton
public class BotMessageHelper {

    public SendMessage createMessage(Message message, String text) {
        return SendMessage.builder()
                .chatId(message.getChatId())
                .replyToMessageId(message.getMessageId())
                .text(text)
                .build();
    }

    public SendVoice createVoice(Message message, String filepath) {
        final InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(filepath);
        final InputFile voiceFile = new InputFile(inputStream, filepath);
        return SendVoice.builder()
                .chatId(message.getChatId())
                .replyToMessageId(message.getMessageId())
                .voice(voiceFile)
                .build();
    }
}
