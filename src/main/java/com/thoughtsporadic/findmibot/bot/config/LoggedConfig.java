package com.thoughtsporadic.findmibot.bot.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class LoggedConfig {
    protected static final Logger log = LoggerFactory.getLogger(LoggedConfig.class);
}
