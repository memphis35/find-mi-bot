package com.thoughtsporadic.findmibot.bot.config;

import com.thoughtsporadic.findmibot.bot.constants.Environments;
import com.thoughtsporadic.findmibot.bot.constants.Senders;
import io.micronaut.context.annotation.Requires;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeDefinition;
import software.amazon.awssdk.services.dynamodb.model.CreateTableRequest;
import software.amazon.awssdk.services.dynamodb.model.DeleteTableRequest;
import software.amazon.awssdk.services.dynamodb.model.KeySchemaElement;
import software.amazon.awssdk.services.dynamodb.model.KeyType;
import software.amazon.awssdk.services.dynamodb.model.ListTablesResponse;
import software.amazon.awssdk.services.dynamodb.model.ProvisionedThroughput;
import software.amazon.awssdk.services.dynamodb.model.ScalarAttributeType;

@Singleton
@Requires(env = Environments.LOCAL)
public class LocalDynamoDbInitializer {

    private static final Logger log = LoggerFactory.getLogger(LocalDynamoDbInitializer.class);

    private final DynamoDbClient dynamoDbClient;

    @Inject
    public LocalDynamoDbInitializer(DynamoDbClient dynamoDbClient) {
        this.dynamoDbClient = dynamoDbClient;
    }

    @PostConstruct
    public void initTable() {
        final ListTablesResponse tables = dynamoDbClient.listTables();
        if (tables.tableNames().contains(Senders.TABLE_NAME)) {
            log.info("Table '{}' exists", Senders.TABLE_NAME);
            final DeleteTableRequest deleteTableRequest = DeleteTableRequest.builder()
                    .tableName(Senders.TABLE_NAME)
                    .build();
            dynamoDbClient.deleteTable(deleteTableRequest);
            log.info("Table '{}' has been deleted", Senders.TABLE_NAME);
        }
        final CreateTableRequest tableRequest = CreateTableRequest.builder()
                .tableName(Senders.TABLE_NAME)
                .attributeDefinitions(this.stringDefinition())
                .keySchema(this.hashKeyElement())
                .provisionedThroughput(ProvisionedThroughput.builder()
                        .readCapacityUnits(5L)
                        .writeCapacityUnits(5L)
                        .build())
                .build();
        dynamoDbClient.createTable(tableRequest);
        log.info("Table '{}' has been created", Senders.TABLE_NAME);
    }

    private AttributeDefinition stringDefinition() {
        return AttributeDefinition.builder()
                .attributeName(Senders.SENDER_ID)
                .attributeType(ScalarAttributeType.S)
                .build();
    }

    private KeySchemaElement hashKeyElement() {
        return KeySchemaElement.builder()
                .attributeName(Senders.SENDER_ID)
                .keyType(KeyType.HASH)
                .build();
    }
}
