package com.thoughtsporadic.findmibot.bot.config;

import io.micronaut.context.annotation.Value;
import jakarta.inject.Singleton;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;

@Singleton
public class AwsProperties {
    public final AwsCredentialsProvider awsCredentialsProvider;
    public final String secretAccessKey;
    public final Region region;

    public AwsProperties(@Value("${aws.access-key-id}") final String accessKeyId,
                         @Value("${aws.secret-access-key}") final String secretAccessKey,
                         @Value("${aws.region}") final String region) {
        this.secretAccessKey = secretAccessKey;
        this.region = Region.of(region);
        final AwsBasicCredentials credentials = AwsBasicCredentials.create(accessKeyId, secretAccessKey);
        this.awsCredentialsProvider = StaticCredentialsProvider.create(credentials);
    }
}
