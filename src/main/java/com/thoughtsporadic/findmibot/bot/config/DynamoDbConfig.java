package com.thoughtsporadic.findmibot.bot.config;

import com.thoughtsporadic.findmibot.bot.constants.Environments;
import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Requires;
import io.micronaut.context.annotation.Value;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

import java.net.URI;

@Factory
@Requires(env = {Environments.LOCAL, Environments.PROD})
public class DynamoDbConfig extends LoggedConfig {

    @Bean
    public DynamoDbClient dynamoDbClient(@Value("${aws.dynamodb.service-endpoint}") final String serviceEndpoint,
                                         AwsProperties awsProperties) {
        final URI dynamoServiceEndpoint = URI.create(serviceEndpoint);
        final DynamoDbClient client = DynamoDbClient.builder()
                .credentialsProvider(awsProperties.awsCredentialsProvider)
                .region(awsProperties.region)
                .endpointOverride(dynamoServiceEndpoint)
                .build();
        log.info("DynamoDbClient is initialized.");
        return client;
    }
}
