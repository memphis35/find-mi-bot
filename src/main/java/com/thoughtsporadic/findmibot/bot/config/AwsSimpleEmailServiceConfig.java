package com.thoughtsporadic.findmibot.bot.config;

import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;
import software.amazon.awssdk.services.ses.SesClient;

@Factory
public class AwsSimpleEmailServiceConfig extends LoggedConfig {

    @Bean
    public SesClient sesClient(AwsProperties awsProperties) {

        final SesClient sesClient = SesClient.builder()
                .credentialsProvider(awsProperties.awsCredentialsProvider)
                .region(awsProperties.region)
                .build();
        log.info("SesClient is initialized");
        return sesClient;
    }
}
