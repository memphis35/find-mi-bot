package com.thoughtsporadic.findmibot.bot.config;

import io.micronaut.context.annotation.Value;
import jakarta.inject.Singleton;

@Singleton
public class TelegramBotProperties {

    public final String botName;
    public final String botToken;

    public TelegramBotProperties(@Value("${bot.name}") final String botName,
                                 @Value("${bot.api-token}") final String botToken) {
        this.botName = botName;
        this.botToken = botToken;
    }
}
