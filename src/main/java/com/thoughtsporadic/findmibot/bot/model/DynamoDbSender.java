package com.thoughtsporadic.findmibot.bot.model;

import com.thoughtsporadic.findmibot.bot.constants.Senders;
import org.telegram.telegrambots.meta.api.objects.User;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class DynamoDbSender {
    private final Map<String, AttributeValue> sender;
    public final Integer messageCount;
    public final boolean isNotified;
    public final boolean isBlocked;

    public DynamoDbSender(Map<String, AttributeValue> values) {
        this.sender = Map.of(
                Senders.SENDER_ID, values.get(Senders.SENDER_ID),
                Senders.FIRST_NAME, values.get(Senders.FIRST_NAME),
                Senders.LAST_NAME, values.get(Senders.LAST_NAME),
                Senders.NICKNAME, values.get(Senders.NICKNAME),
                Senders.MESSAGE_COUNT, values.get(Senders.MESSAGE_COUNT),
                Senders.IS_NOTIFIED, values.get(Senders.IS_NOTIFIED),
                Senders.IS_BLOCKED, values.get(Senders.IS_BLOCKED));
        this.isNotified = values.get(Senders.IS_NOTIFIED).bool();
        this.isBlocked = values.get(Senders.IS_BLOCKED).bool();
        this.messageCount = Integer.parseInt(values.get(Senders.MESSAGE_COUNT).n());
    }

    public DynamoDbSender(User sender) {
        this.sender = Map.of(
                Senders.SENDER_ID, AttributeValue.fromN(sender.getId().toString()),
                Senders.FIRST_NAME, AttributeValue.fromS(sender.getFirstName()),
                Senders.LAST_NAME, AttributeValue.fromS(Objects.requireNonNullElse(sender.getLastName(), "-")),
                Senders.NICKNAME, AttributeValue.fromS(Objects.requireNonNullElse(sender.getUserName(), "-")),
                Senders.MESSAGE_COUNT, AttributeValue.fromN(String.valueOf(0)),
                Senders.IS_NOTIFIED, AttributeValue.fromBool(false),
                Senders.IS_BLOCKED, AttributeValue.fromBool(false)
        );
        this.isNotified = false;
        this.isBlocked = false;
        this.messageCount = 0;
    }

    public Map<String, AttributeValue> getAsAttributes() {
        return Map.copyOf(sender);
    }

    public DynamoDbSender setNotified() {
        final Map<String, AttributeValue> copied = new HashMap<>(sender);
        copied.put(Senders.IS_NOTIFIED, AttributeValue.fromBool(true));
        copied.put(Senders.MESSAGE_COUNT, AttributeValue.fromN(Integer.toString(messageCount + 1)));
        return new DynamoDbSender(copied);
    }

    public DynamoDbSender setBlocked() {
        final Map<String, AttributeValue> copied = new HashMap<>(sender);
        copied.put(Senders.IS_BLOCKED, AttributeValue.fromBool(true));
        return new DynamoDbSender(copied);
    }

    public DynamoDbSender addMessage() {
        final Map<String, AttributeValue> copied = new HashMap<>(sender);
        copied.put(Senders.MESSAGE_COUNT, AttributeValue.fromN(Integer.toString(messageCount + 1)));
        return new DynamoDbSender(copied);
    }
}
