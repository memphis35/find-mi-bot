package com.thoughtsporadic.findmibot.bot.dao;

import com.thoughtsporadic.findmibot.bot.constants.Senders;
import com.thoughtsporadic.findmibot.bot.model.DynamoDbSender;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.objects.User;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.GetItemRequest;
import software.amazon.awssdk.services.dynamodb.model.GetItemResponse;
import software.amazon.awssdk.services.dynamodb.model.PutItemRequest;

import java.util.Map;
import java.util.Optional;

@Singleton
public class DynamoDbBotDao implements BotDao {

    private static final Logger log = LoggerFactory.getLogger(DynamoDbBotDao.class);

    private final DynamoDbClient dynamoDbClient;

    @Inject
    public DynamoDbBotDao(DynamoDbClient dynamoDbClient) {
        this.dynamoDbClient = dynamoDbClient;
    }

    @Override
    public void saveUser(final User contact) {
        final DynamoDbSender dynamoDbSender = new DynamoDbSender(contact);
        final PutItemRequest request = PutItemRequest.builder()
                .tableName(Senders.TABLE_NAME)
                .item(dynamoDbSender.getAsAttributes())
                .build();
        dynamoDbClient.putItem(request);
        log.info("User #{} saved.", contact.getId());
    }

    @Override
    public int countMessage(final Long id) {
        return this.getUser(id).map(sender -> {
            final DynamoDbSender notifiedSender = sender.addMessage();
            final PutItemRequest request = PutItemRequest.builder()
                    .tableName(Senders.TABLE_NAME)
                    .item(notifiedSender.getAsAttributes())
                    .build();
            dynamoDbClient.putItem(request);
            return notifiedSender.messageCount;
        }).orElse(-1);
    }

    @Override
    public void blockUser(final Long id) {
        this.getUser(id).ifPresent(sender -> {
            final DynamoDbSender notifiedSender = sender.setBlocked();
            final PutItemRequest request = PutItemRequest.builder()
                    .tableName(Senders.TABLE_NAME)
                    .item(notifiedSender.getAsAttributes())
                    .build();
            dynamoDbClient.putItem(request);
        });
    }

    @Override
    public void notifyUser(final Long id) {
        this.getUser(id).ifPresent(sender -> {
            final DynamoDbSender notifiedSender = sender.setNotified();
            final PutItemRequest request = PutItemRequest.builder()
                    .tableName(Senders.TABLE_NAME)
                    .item(notifiedSender.getAsAttributes())
                    .build();
            dynamoDbClient.putItem(request);
        });
    }

    @Override
    public Optional<DynamoDbSender> getUser(final Long id) {
        final GetItemRequest request = GetItemRequest.builder()
                .tableName(Senders.TABLE_NAME)
                .key(Map.of(Senders.SENDER_ID, AttributeValue.fromN(id.toString())))
                .build();
        final GetItemResponse item = dynamoDbClient.getItem(request);
        return (item.hasItem())
                ? Optional.ofNullable(item.item())
                .map(DynamoDbSender::new)
                : Optional.empty();
    }
}
