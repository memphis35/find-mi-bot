package com.thoughtsporadic.findmibot.bot.dao;

import com.thoughtsporadic.findmibot.bot.model.DynamoDbSender;
import org.telegram.telegrambots.meta.api.objects.User;

import java.util.Optional;

public interface BotDao {

    Optional<DynamoDbSender> getUser(Long id);

    void saveUser(User contact);

    int countMessage(Long id);

    void blockUser(Long id);

    void notifyUser(Long id);
}
