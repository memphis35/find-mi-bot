package com.thoughtsporadic.findmibot.bot.constants;

public class Messages {

    public static final String GREETING = """
            Hi. Before you start to type something to me, I want to mention I am not going to message you
            right now. Instead of this, I will receive a notification that you would like to contact me. Eventually,
            I will decide whether it is necessary to answer you. I thank you and wish you a good day.
            """;

    public static final String CONFIRMATION = "Your message has been sent. Thank you <3";

    public static final int MESSAGE_LIMIT = 2;

    public static final String SUBJECT = "Someone is trying to contact you";

    public static final String BODY_TEMPLATE = """
            User ID: %s
            Name: %s '%s' %s
            Message: %s
            """;
}
