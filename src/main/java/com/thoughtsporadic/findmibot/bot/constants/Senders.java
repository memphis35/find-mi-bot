package com.thoughtsporadic.findmibot.bot.constants;

public class Senders {
    public static final String TABLE_NAME = "senders";
    public static final String SENDER_ID = "sender_id";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String NICKNAME = "nickname";
    public static final String IS_NOTIFIED = "is_notified";
    public static final String IS_BLOCKED = "is_blocked";
    public static final String MESSAGE_COUNT = "message_count";

}
