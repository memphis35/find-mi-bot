package com.thoughtsporadic.findmibot.bot.constants;

public class Environments {
    public static final String TEST = "test";
    public static final String LOCAL = "local";
    public static final String PROD = "production";
}
