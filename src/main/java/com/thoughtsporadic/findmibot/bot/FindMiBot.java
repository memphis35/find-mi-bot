package com.thoughtsporadic.findmibot.bot;

import com.thoughtsporadic.findmibot.bot.config.TelegramBotProperties;
import com.thoughtsporadic.findmibot.bot.constants.Messages;
import com.thoughtsporadic.findmibot.bot.dao.BotDao;
import com.thoughtsporadic.findmibot.bot.sender.EmailSender;
import com.thoughtsporadic.findmibot.bot.util.BotMessageHelper;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendVoice;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.net.URISyntaxException;

@Singleton
public class FindMiBot extends TelegramLongPollingBot {

    private static final Logger log = LoggerFactory.getLogger(FindMiBot.class);

    private final String botName;

    private final BotDao botDao;
    private final BotMessageHelper helper;
    private final EmailSender emailSender;

    @Inject
    public FindMiBot(TelegramBotProperties botProperties,
                     @Named("dynamoDb") final BotDao botDao,
                     @Named("awsSes") final EmailSender emailSender,
                     final BotMessageHelper helper) {
        super(botProperties.botToken);
        this.botName = botProperties.botName;
        this.botDao = botDao;
        this.helper = helper;
        this.emailSender = emailSender;
    }

    @Override
    public String getBotUsername() {
        return botName;
    }

    @Override
    public void onUpdateReceived(final Update update) {
        final Message original = update.getMessage();
        final User fromUser = original.getFrom();
        botDao.getUser(fromUser.getId()).ifPresentOrElse(sender -> {
            try {
                if (!sender.isBlocked) {
                    if (!sender.isNotified) {
                        this.notifySender(fromUser, original);
                    } else if (sender.messageCount > Messages.MESSAGE_LIMIT) {
                        this.blockSender(fromUser, original);
                    } else {
                        botDao.countMessage(fromUser.getId());
                    }
                }
            } catch (Exception e) {
                log.error("Caught an exception while handling communication: {}", e.getMessage(), e);
            }
        }, () -> this.saveNewSender(fromUser, original));
    }

    private void saveNewSender(User fromUser, Message original) {
        try {
            botDao.saveUser(fromUser);
            final SendMessage greetingMessage = helper.createMessage(original, Messages.GREETING);
            this.execute(greetingMessage);
        } catch (Exception e) {
            log.error("Caught an exception while handling communication: {}", e.getMessage(), e);
        }
    }

    private void notifySender(User fromUser, Message original) throws TelegramApiException {
        emailSender.sendEmail(fromUser, original.getText());
        botDao.notifyUser(fromUser.getId());
        botDao.countMessage(fromUser.getId());
        final SendMessage confirmMessage = helper.createMessage(original, Messages.CONFIRMATION);
        this.execute(confirmMessage);
    }

    private void blockSender(User fromUser, Message original) throws TelegramApiException, URISyntaxException {
        botDao.blockUser(fromUser.getId());
        final SendVoice voice = helper.createVoice(original, "voice.mpga");
        this.execute(voice);
    }
}
