package com.thoughtsporadic.findmibot.bot.exception;

public class FindMiBotException extends RuntimeException {
    public FindMiBotException(String msg, Throwable e) {
        super(msg, e);
    }
}
