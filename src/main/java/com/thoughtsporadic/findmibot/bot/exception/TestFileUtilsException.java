package com.thoughtsporadic.findmibot.bot.exception;

public class TestFileUtilsException extends FindMiBotException {
    public TestFileUtilsException(String msg, Throwable e) {
        super(msg, e);
    }
}
