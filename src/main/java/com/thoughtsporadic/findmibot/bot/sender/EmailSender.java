package com.thoughtsporadic.findmibot.bot.sender;

import org.telegram.telegrambots.meta.api.objects.User;

public interface EmailSender {

    void sendEmail(User fromUser, String message);
}
