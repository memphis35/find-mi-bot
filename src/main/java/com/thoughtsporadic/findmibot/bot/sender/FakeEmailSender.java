package com.thoughtsporadic.findmibot.bot.sender;

import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.objects.User;

@Singleton
public class FakeEmailSender implements EmailSender {

    private static final Logger log = LoggerFactory.getLogger(FakeEmailSender.class);

    @Override
    public void sendEmail(User fromUser, String message) {
        log.info("Email has been sent: {}", message);
    }
}
