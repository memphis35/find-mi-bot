package com.thoughtsporadic.findmibot.bot.sender;

import com.thoughtsporadic.findmibot.bot.constants.Environments;
import com.thoughtsporadic.findmibot.bot.constants.Messages;
import io.micronaut.context.annotation.Requires;
import io.micronaut.context.annotation.Value;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import org.telegram.telegrambots.meta.api.objects.User;
import software.amazon.awssdk.services.ses.SesClient;
import software.amazon.awssdk.services.ses.model.Body;
import software.amazon.awssdk.services.ses.model.Content;
import software.amazon.awssdk.services.ses.model.Destination;
import software.amazon.awssdk.services.ses.model.Message;
import software.amazon.awssdk.services.ses.model.SendEmailRequest;

@Singleton
@Requires(env = Environments.PROD)
public class AwsSesEmailSender implements EmailSender {

    private final SesClient sesClient;
    private final String source;
    private final String recipient;
    private final String sourceArn;

    @Inject
    public AwsSesEmailSender(SesClient sesClient,
                             @Value("${aws.ses.source}") String source,
                             @Value("${aws.ses.recipient}") String recipient,
                             @Value("${aws.ses.source-arn}") String sourceArn) {
        this.sesClient = sesClient;
        this.source = source;
        this.recipient = recipient;
        this.sourceArn = sourceArn;
    }

    @Override
    public void sendEmail(User sender, String message) {
        final String bodyText = Messages.BODY_TEMPLATE
                .formatted(sender.getId(), sender.getFirstName(), sender.getUserName(), sender.getLastName(), message);

        final Content subject = Content.builder().data(Messages.SUBJECT).build();
        final Content body = Content.builder().data(bodyText).build();
        final Body emailBody = Body.builder().text(body).build();
        final Message emailMessage = Message.builder().subject(subject).body(emailBody).build();
        final Destination destination = Destination.builder().toAddresses(recipient).build();

        final SendEmailRequest request = SendEmailRequest.builder()
                .source(source)
                .sourceArn(sourceArn)
                .destination(destination)
                .message(emailMessage)
                .build();
        sesClient.sendEmail(request);
    }
}
