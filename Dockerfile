FROM amazoncorretto:17.0.7-al2023@sha256:b65499edfccce124ecf6e0283ea46f193b6abf83fdc66d2b815ee38bcaf0dced

ENV MICRONAUT_ENVIRONMENTS=production

RUN mkdir /app && chown -R adm:adm /app

WORKDIR /app
USER adm

COPY ./build/libs/find-mi-bot.jar .

ENTRYPOINT ["java", "-jar", "/app/find-mi-bot.jar"]